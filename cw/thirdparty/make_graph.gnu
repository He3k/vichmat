set termoption enhanced
set terminal png size 1600,1200 font "Arial, 16"
set output "graph.png"
set border lw 2
set grid
set xlabel "Дни"
set ylabel "Люди" rotate by 90
set format x "%6.0f"
set format y "%.0f"
plot 'result/out.txt' using 1:5 with lines lw 2 title "Зараженные"

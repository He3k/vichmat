#include "function.h"


const double a_E = 0.999, a_I = 0.999, k = 0.042, p = 0.952, b = 0.999,
            u = 0.0188, c_isol = 0, y = 0, a = 1;

double fc = 1 + c_isol*(1 - 2/5 * a);

double f_SE(double S, double E, double I, double N) {
	return 	a_I*S*I/N + a_E*S*E/N;
}

double f_S(double S, double E, double I, double R, double N)
{
	return -1 * fc * f_SE(S, E, I, N) + y*R;
}

double f_E( double S, double E, double I, double N)
{
	return fc * f_SE(S, E, I, N) - (k + p)*E;
}

double f_I(double E, double I)
{
	return k*E - b*I - u*I;
}

double f_R(double E, double I, double R)
{
	return b*I + p*E - y*R;
}

double f_D(double I)
{
	return u*I;
}

double computingS(double S, double E, double I, double R, double N, double h)
{
	return h * f_S(S + h/2 * f_S(S,E,I,R,N), E + h/2, I+h/2, R+h/2, N+h/2);
}

double computingE(double S, double E, double I, double N, double h) 
{
	return h * f_E(S+h/2, E + h/2 * f_E(S,E,I,N), I+h/2, N+h/2);
}

double computingI(double E, double I, double h)
{
	return h * f_I(E+h/2, I + h/2*f_I(E, I));
}

double computingR(double E, double I, double R, double h)
{
	return h * f_R(E+h/2, I+h/2, R + h/2*f_R(E,I,R));
}

double computingD(double I, double h)
{
	return h * f_D(I+h/2);
}
#include <stdio.h>
#include <stdlib.h>

double computingS(double S, double E, double I, double R, double N, double h);
double computingE(double S, double E, double I, double N, double h);
double computingI(double E, double I, double h);
double computingR(double E, double I, double R, double h);
double computingD(double I, double h);
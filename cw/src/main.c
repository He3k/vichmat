#include "function.h"


int main() {

    double t0 = 0, T = 90, h = 1;
	int n = (T - t0) / h + 1;
    double S[n], E[n], I[n], R[n], D[n], N[n], t[n];

    E[0] = 99;
	R[0] = 24;
	S[0] = 2798170 - E[0] - R[0];
	I[0] = 0;
	D[0] = 0;
	N[0] = S[0] + E[0] + I[0] + R[0] + D[0];
	t[0] = t0;

    FILE *file_out;
    file_out = fopen("../result/out.txt", "w+");

    for(int i = t0+1; i < n; i++) {
		S[i] = S[i-1] + computingS(S[i-1], E[i-1], I[i-1], R[i-1], N[i-1], h);
		E[i] = E[i-1] + computingE(S[i-1], E[i-1], I[i-1], N[i-1], h);
		I[i] = I[i-1] + computingI(E[i-1], I[i-1], h);
		R[i] = R[i-1] + computingR(E[i-1], I[i-1], R[i-1], h);
		D[i] = D[i-1] + computingD(I[i-1], h);
		N[i] = S[i] + E[i] + I[i] + R[i] + D[i];
		t[i] = t0 + i*h;
		fprintf(file_out,"%.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f\n",  t[i], N[i], S[i], E[i], I[i], R[i], D[i]);
	}
    fclose(file_out);
}

set termoption enhanced
set terminal png size 1600,1200 font "Arial, 16"
set output "graph.png"
set border lw 2
set grid
set key top left
set xlabel "Количество элементов в массиве"
set ylabel "Время выполнения, с" rotate by 90
#set xrange [-1: 10]
#set yrange [-1: 10]
set format x "%6.0f"
set format y "%.2f"
plot 'bin/in.txt' using 1:2 with lines lw 1 lt rgb 'red'

#include <iostream>
#include <fstream>
#include <cmath>

#define N 3
double esp = 0.001;

using namespace std;

double *gauss(double **a, int n);
double* step_roots(double **C, double *roots_prev, int n);
int accuracy(double *roots_prev, double *roots, int n);

int main()
{
	ifstream fin("in.txt");  // open file

	if(!fin.is_open()) {
		cout << "File can't open!\n";
		exit(1);
	}

	double **arr = new double *[N];
	for(int i = 0; i < N; i++) 
		arr[i] = new double [N+1];

	for(int i = 0; i < N; i++) {
		for(int j = 0; j < N + 1; j++)
			fin >> arr[i][j];
	}

	for(int i = 0; i < N; i++) {
		for(int j = 0; j < N + 1; j++)
			cout << arr[i][j] << " ";
		cout <<"\n";
	}

	double* out = gauss(arr, N);

	ofstream fout("out.txt");   // close file
	for(int i = 0; i < N; i ++) {
		fout << "x" << i+1 << " = " << out[i] <<"\n";
	}
}

double *gauss(double **a, int n)
{
	double *roots = new double [n];
	double *roots_prev = new double [n];
	int c;

	for(int i = 0; i < n; i++) {
		c = a[i][i];
		for(int j = 0; j < n + 1; j++) {
			if(i == j)
				a[i][j] = 0;
			else
				a[i][j] /= c;
		}
	}

	for(int i = 0; i < n; i++)
		roots_prev[i] = 0;

	while(1) {
		roots = step_roots(a, roots_prev, n);
		if(!accuracy(roots_prev, roots, n))
			break;
		roots_prev = roots;

	}
	return roots;
}

double* step_roots(double **C, double *roots_prev, int n)
{
	double *roots = new double [n];

	for(int i = 0; i < n; i++)
		roots[i] = 0;

	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			
			roots[i] += C[i][j] * roots_prev[j];
		}
	}

	for(int i = 0; i < n; i++) {
		roots[i] = C[i][n] - roots[i];
	}

	return roots;
}

int accuracy(double *roots_prev, double *roots, int n)
{
	for(int i = 0; i < n; i++) {
		if(abs(roots[i] - roots_prev[i]) >= esp)
			return 1;
	}
	return 0;
}
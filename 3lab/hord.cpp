#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

double eps = 0.00000001;

double f(double x);
double findRoot(double a, double b);

int main()
{
	ifstream fin("in.txt");
	if(!fin.is_open()) {
		cout << "Файл не может быть открыт!\n";
		exit(1);
	}

	double left;
	double right;

	fin >> left;
	fin >> right;

	double out = findRoot(left, right);

	ofstream fout("out.txt");
	fout << out;

}

double f(double x) 
{
	return x*x - 3;
}

double findRoot(double a, double b)
{
	double c;
	while(abs(a-b) > eps) {
		c = (a*f(b) - b*f(a)) / (f(b) - f(a));
		if(f(a)*f(c) < 0)
			b = c;
		else
			a = c;
	}
	cout << a << " " << b << endl;
	return c;
}

//c = (a*f(b) - b*f(a)) / f(b) - f(a);
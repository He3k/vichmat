#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

double eps = 0.00001;

double f(double x);
double df(double x);
double findX0(double a, double b, double ddf_sign);
double solve(double x0);

int main()
{
	ifstream fin("in.txt");
	if(!fin.is_open()) {
		cout << "Файл не может быть открыт!\n";
		exit(1);
	}

	double left;
	double right;

	fin >> left;
	fin >> right;

	double x0 = findX0(left,right, 1);
	if(x0 == -1)
		x0 = left;
	else if(x0 = 1)
		x0 = right;
	else {
		printf("govno\n");
		return -1;
	}

	double out = solve(x0);

	ofstream fout("out.txt");
	fout << out;

}

double f(double x) 
{
	return x*x - 3;
}

double df(double x)
{
	return 2*x;
}

double findX0(double a, double b, double ddf_sign)
{
	a = f(a);
	b = f(b);

	if(ddf_sign * a > 0)
		return -1;
	if(ddf_sign * b > 0)
		return 1;
	return 0;
}

double solve(double x0)
{
	double x1 = x0 - f(x0) / df(x0);
	while(abs(x1 - x0) > eps) {
		x0 = x1;
		x1 = x0 - f(x0) / df(x0);
	}
	return x1;
}
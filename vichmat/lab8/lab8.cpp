#include <iostream>
#include <cmath>
#include <fstream>
#include <stdlib.h>

using namespace std;

double f(double x);
double monte_carlo(double maxX, double maxY);
double get_random_number_float(double min, double max, double precision);

int main()
{
	ofstream result("out.txt");
	result << monte_carlo(4,16);
}

double f(double x)
{
	return x*x;
}

double monte_carlo(double maxX, double maxY)
{
	ofstream inside("inside.txt");
	ofstream outside("outside.txt");

	int n = 1000000;
	int in = 0;
	double x, y;
	for(int i = 0; i < n; i++) {
		x = get_random_number_float(0, maxX, 4);
		y = get_random_number_float(0, maxY, 4);
		if(y <= f(x)) {
			in++;
			inside << x << " " << y << "\n";
		} 
		else
			outside << x << " " << y << "\n";
	}
	return (double)in / n * maxX * maxY;
}

double get_random_number_float(double min, double max, double precision)
{
	double value;

	value = rand() % (int)pow(10, precision);
	value = min + (value / pow(10, precision)) * (max - min);

	return value;
}
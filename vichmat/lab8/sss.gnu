set termoption enhanced
set terminal png size 800,600 font "Arial, 16"
set output "plot2.png"
set style line 1 lc rgb "0xDC143C" lt 1 lw 4 pt 9 ps 1
set style line 2 lc rgb "0x6495ED" lt 1 lw 4 pt 7 ps 1
set border lw 2
set grid
set key top outside
set xlabel "x"
set ylabel "y" rotate by 90
#set xrange [-1: 10]
#set yrange [-1: 10]
#set format x "%6.0f"
#set format y "%.2f"
plot 'inside.txt' using 1:2 with points lw 1 pt 7 lt rgb 'dark-violet' title "points inside", 'outside.txt' using 1:2 with points lw 1 pt 7 lt rgb 'purple' title "points outside"
#include "new_method.h"

double esp = 0.001;

void fill_matrix(char* check) {
    int i,j,size;
    FILE *fi;                                   //
    if ((fi = fopen(check, "r")) != NULL) {     // check
          while (!ferror(fi) && !feof(fi))  {       // 
        if (fgetc(fi) == '\n')                  // strings
        size++;                                 //
      }                                         // file
        //printf("count >> %d\n", size);        
        fclose(fi);                             //
        } else {                                    //
                printf("\033[1;31mWhich file should I open, genius?\033[0m\n");
                exit(0);                                //
        }                                           //

        float matrix[size][size + 1];               // init matrix

    FILE *file;                                 //
    file = fopen(check, "r");                   //
        for (i = 0; i < size; i++) {                // fill
                for (j = 0; j < size + 1; j++) {        // 
                        fscanf(file,"%f",&matrix[i][j]);    // matrix
                }                                       //
        }                                           //
    fclose(file);
    double* out = gauss(matrix, size);
    print(size, matrix);
    out_in_file(size, out);  // вывод массива

}

void print(int size, float matrix[][size+1]) {
        int j;
    for (int i = 0; i < size; i++) {                //
                for (j = 0; j < size + 1; j++){         // 
                        printf("  %.0f  ", matrix[i][j]);        // print
                }                                       // 
                printf("\n");                           // matrix
                j = 0;                                  //
        }                                           //
        printf("\n");
}


double *gauss(double **a, int n)
{
	double *roots = new double [n];
	double *roots_prev = new double [n];
	int c;

	for(int i = 0; i < n; i++) {
		c = a[i][i];
		for(int j = 0; j < n + 1; j++) {
			if(i == j)
				a[i][j] = 0;
			else
				a[i][j] /= c;
		}
	}

	for(int i = 0; i < n; i++)
		roots_prev[i] = 0;

	while(1) {
		roots = step_roots(a, roots_prev, n);
		if(!accuracy(roots_prev, roots, n))
			break;
		roots_prev = roots;

	}
	return roots;
}

double* step_roots(double **C, double *roots_prev, int n)
{
	double *roots = new double [n];

	for(int i = 0; i < n; i++)
		roots[i] = 0;

	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			
			roots[i] += C[i][j] * roots_prev[j];
		}
	}

	for(int i = 0; i < n; i++) {
		roots[i] = C[i][n] - roots[i];
	}

	return roots;
}

int accuracy(double *roots_prev, double *roots, int n)
{
	for(int i = 0; i < n; i++) {
		if(abs(roots[i] - roots_prev[i]) >= esp)
			return 1;
	}
	return 0;
}

void out_in_file(int size, double out) {
        FILE *file_out;
        file_out = fopen("out.txt", "w+");
        printf("Result: \n" );
        for (int i = 0; i < size; i++) {
                printf ("X%d =  %.0f\n", i + 1, vars[i]);
        fprintf(file_out, "x =%6.2f\n", out[i]);
    }
    fclose(file_out); 
}
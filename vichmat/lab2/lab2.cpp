#include <iostream>
#include <fstream>
#include <cmath>

#define N 3

using namespace std;

double *rsly_gauss(double **a, int n);
double* step_roots_solution(double **C, double *roots_prev, int n);

int main()
{
	ifstream fin("in_old.txt");
	if(!fin.is_open()) {
		cout << "Файл не может быть открыт!\n";
		exit(1);
	}
	double **arr = new double *[N];
	for(int i = 0; i < N; i++) 
		arr[i] = new double [N+1];

	for(int i = 0; i < N; i++) {
		for(int j = 0; j < N + 1; j++)
			fin >> arr[i][j];
	}

	for(int i = 0; i < N; i++) {
		for(int j = 0; j < N + 1; j++)
			cout << arr[i][j] << " ";
		cout <<"\n";
	}

	double* out = rsly_gauss(arr, N);

	ofstream fout("out.txt");
	for(int i = 0; i < N; i ++) {
		fout << "x" << i+1 << " = " << out[i] <<"\n";
	}

}

double *rsly_gauss(double **a, int n)
{
	double *roots = new double [n];
	double *roots_prev = new double [n];
	int c;

	for(int i = 0; i < n; i++) {
		roots[i] = a[i][n];
		roots_prev[i] = 0;
	}

	for(int i = 0; i < n; i++) {
		c = a[i][i];
		for(int j = 0; j < n + 1; j++) {
			if(c)
				a[i][j] = a[i][j] / c;
		}
		a[i][i] = 0;
	}

	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n + 1; j++)
			cout << a[i][j] << " ";
		cout << endl;
	}

	int max;
	int esp = 0.00001;

	while(1) {
		roots = step_roots_solution(a, roots_prev, n);
		max = roots[0];
		for(int i = 1; i < n; i++)
		{
			if(roots[i] > max)
				max = roots[i];
		}
		if(max < esp)
			break;

		for(int i = 0; i < n; i++)
			roots_prev[i] = roots[i];

	}
	for(int i = 0; i < n; i++)
		cout << roots[i] << endl;
	return roots;
}

double* step_roots_solution(double **C, double *roots_prev, int n)
{
	double *roots = new double [n];
	for(int i = 0; i < n; i ++)
		roots[i] = 0;
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++){
			roots[i] = roots_prev[i] * C[i][j];
			C[i][j] = C[i][j] * roots_prev[i];
		}
	}
	for(int i = 0; i < n; i++) {
		roots[i] = C[i][n] - roots[i];
	}

	return roots;
}
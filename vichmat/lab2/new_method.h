#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void fill_matrix(char* check);
void print(int size, float matrix[][size+1]);
double *gauss(double **a, int n);
double* step_roots(double **C, double *roots_prev, int n);
int accuracy(double *roots_prev, double *roots, int n);

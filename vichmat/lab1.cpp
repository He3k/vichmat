#include <iostream>
#include <fstream>
#include <cmath>

#define N 3

using namespace std;

double *rsly_gauss(double **a, int n);

int main()
{
	ifstream fin("in.txt");
	if(!fin.is_open()) {
		cout << "Файл не может быть открыт!\n";
		exit(1);
	}
	double **arr = new double *[N];
	for(int i = 0; i < N; i++) 
		arr[i] = new double [N+1];

	for(int i = 0; i < N; i++) {
		for(int j = 0; j < N + 1; j++)
			fin >> arr[i][j];
	}

	for(int i = 0; i < N; i++) {
		for(int j = 0; j < N + 1; j++)
			cout << arr[i][j] << " ";
		cout <<"\n";
	}

	double* out = rsly_gauss(arr, N);

	ofstream fout("out.txt");
	for(int i = 0; i < N; i ++) {
		fout << "x" << i+1 << " = " << out[i] <<"\n";
	}

}

double *rsly_gauss(double **a, int n)
{
	int imax;
	double max, c;
	double esp = 0.00001;

	for(int k = 0; k < n; k++) {
		imax = k;
		max = fabs(a[k][k]);

		for(int j = k; j < n; j++) { // Поиск макс. элемента на диагонали
			if(fabs(a[j][k]) > max) {
				max = a[j][k];
				imax = j;
			}
		}

		if(max == 0) {    //Если столбец нулей
			cout << "jopa\n";
			exit(2);
		}

		if(imax != k) { // Свап строк
			double tmp;
			for(int j = k; j < n + 1; j++) {
				tmp = a[k][j];
				a[k][j] = a[imax][j];
				a[imax][j] = tmp;
			}
		}

		for(int i = k; i < n; i++) { 
			c = a[i][k];
			if(c) {
				for(int j = k; j < n + 1; j++) {
					a[i][j] = a[i][j] / c;
				}
			}
		}

		for(int i = k + 1; i < n; i++) {
			for(int j = k; j < n + 1; j++) {
				a[i][j] -= a[k][j];
			}
		}
	}

	double *out = new double [n];

	if(fabs(a[n-1][n]) < esp)
		a[n-1][n] = 0;
	out[n-1] = a[n-1][n];

	int counter = 1;

	for(int i = n - 2; i >= 0; i--) {
		for(int j = n - 1; j > n - 1 - counter; j--) {
			a[i][n] -= a[i][j]*out[j];
		}
		if(fabs(a[i][n]) < esp)
			a[i][n] = 0;
		out[i] = a[i][n];
		counter++;
	}
	return out;
}


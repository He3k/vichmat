#include <stdio.h>
#include <stdlib.h>


const double a_E = 0.999, a_I = 0.999, k = 0.042, p = 0.952, b = 0.999,
             u = 0.0188, c_isol = 0, y = 0, a = 1;

double euler_S(double S, double E, double I, double R, double N, double h);
double euler_E(double S, double E, double I, double N, double h);
double euler_I(double E, double I, double h);
double euler_R(double E, double I, double R, double h);
double euler_D(double I, double h);

double fc = 1 + c_isol*(1 - 2/5 * a);

int main() {

    double t0 = 0, T = 90, h = 1;
	int n = (T - t0) / h + 1;
    double S[n], E[n], I[n], R[n], D[n], N[n], t[n];

    E[0] = 99;
	R[0] = 24;
	S[0] = 2798170 - E[0] - R[0];
	I[0] = 0;
	D[0] = 0;
	N[0] = S[0] + E[0] + I[0] + R[0] + D[0];
	t[0] = t0;

    FILE *file_out;
    file_out = fopen("result.txt", "w+");

    for(int i = t0+1; i < n; i++) {
		S[i] = S[i-1] + euler_S(S[i-1], E[i-1], I[i-1], R[i-1], N[i-1], h);
		E[i] = E[i-1] + euler_E(S[i-1], E[i-1], I[i-1], N[i-1], h);
		I[i] = I[i-1] + euler_I(E[i-1], I[i-1], h);
		R[i] = R[i-1] + euler_R(E[i-1], I[i-1], R[i-1], h);
		D[i] = D[i-1] + euler_D(I[i-1], h);
		N[i] = S[i] + E[i] + I[i] + R[i] + D[i];
		t[i] = t0 + i*h;
		fprintf(file_out,"%.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f\n",  t[i], N[i], S[i], E[i], I[i], R[i], D[i]);
	}
    fclose(file_out);
}



double f_SE(double S, double E, double I, double N) {
	return 	a_I*S*I/N + a_E*S*E/N;
}

double f_S(double S, double E, double I, double R, double N)
{
	return -1 * fc * f_SE(S, E, I, N) + y*R;
}

double f_E(double S, double E, double I, double N)
{
	return fc * f_SE(S, E, I, N) - (k + p)*E;
}

double f_I(double E, double I)
{
	return k*E - b*I - u*I;
}

double f_R(double E, double I, double R)
{
	return b*I + p*E - y*R;
}

double f_D(double I)
{
	return u*I;
}

double euler_S(double S, double E, double I, double R, double N, double h)
{
	return h * f_S(S + h/2 * f_S(S,E,I,R,N), E + h/2, I+h/2, R+h/2, N+h/2);
}

double euler_E(double S, double E, double I, double N, double h) 
{
	return h * f_E(S+h/2, E + h/2 * f_E(S,E,I,N), I+h/2, N+h/2);
}

double euler_I(double E, double I, double h)
{
	return h * f_I(E+h/2, I + h/2*f_I(E, I));
}

double euler_R(double E, double I, double R, double h)
{
	return h * f_R(E+h/2, I+h/2, R + h/2*f_R(E,I,R));
}

double euler_D(double I, double h)
{
	return h * f_D(I+h/2);
}
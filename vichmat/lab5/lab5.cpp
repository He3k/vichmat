#include <iostream>
#include <fstream>

using namespace std;

double calc_start(double * xin, double * yin, double x, int n);
double calc(double* xin, double *yin,double x, int s, int f);
void bubble_sort(double* x,double* y,int n);

int main()
{
	int num_of_xy = 4;
	// double xin[num_of_xy + 1] = {1.5, 1.52, 1.54, 1.56, 1.58};
	// double yin[num_of_xy + 1] = {4.4817, 4.5722, 4.6646, 4.7588, 4.855};
	double xin[num_of_xy + 1] = {1, 2, 3, 4};
	double yin[num_of_xy + 1] = {1, 1.4142, 1.7321, 2};
	cout << "Введите икс: ";
	double x;
	cin >> x;
	xin[num_of_xy] = x;
	yin[num_of_xy] = calc_start(xin, yin, x, num_of_xy);
	bubble_sort(xin, yin, num_of_xy + 1);
	
	ofstream fout("out.txt");
	for(int i = 0; i < num_of_xy + 1; i ++) {
		fout << xin[i] <<" " << yin[i] <<"\n";
	}

}

double calc_start(double * xin, double * yin, double x, int n)
{
	return calc(xin, yin, x, 0, n-1);
}

double calc(double* xin, double *yin,double x, int s, int f)
{
	if(s == f)
		return yin[s];

	return ((calc(xin, yin, x, s, f-1) * (x - xin[f])) - (calc(xin, yin,x, s+1, f) * (x - xin[s]))) / (xin[s] - xin[f]);
}

void bubble_sort(double* x,double* y,int n)
{
	float b;
	float c;
	for (int i=0; i < n - 1; i++)
	{
     	for (int j=0; j < n -1; j++) { 
         	if (x[j] > x[j+1])
         	{
        	 	b = x[j];
        	 	c = y[j];
        	 	x[j] = x[j+1];
        	 	y[j] = y[j+1];
        	 	x[j+1] = b;
        	 	y[j+1] = c;
			}
		}
	}
}
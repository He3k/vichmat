#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

double f(double x, double y);

int main()
{
	double a,b,h;

	cout << "Введите нижний предел интегрирования: ";
	cin >> a;
	cout << "Введите верхний предел интегрирования: ";
	cin >> b;
	cout << "Введите шаг: ";
	cin >> h;
	int n = (b - a) / h;
	double x[n+1];
	double y[n+1];
	double y1[n+1];
	x[0] = a;
	y[0] = 1;
	ofstream fout("out.txt");

	for(int i = 1; i <= n; i++) {
		x[i] = a + i*h;
		y1[i] = y[i-1] + h*f(x[i-1], y[i-1]);
		y[i] = y[i-1] + h*(f(x[i-1], y[i-1])+f(x[i],y1[i]))/2;
		fout << x[i-1] << " " << y[i-1] << "\n";
	}
	fout << x[n] << " " << y[n] << "\n";

}

double f(double x, double y)
{
	return x+y;
}
#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

double f(double x, double y);

int main()
{
	double a,b,h;

	cout << "Введите нижний предел интегрирования: ";
	cin >> a;
	cout << "Введите верхний предел интегрирования: ";
	cin >> b;
	cout << "Введите шаг: ";
	cin >> h;
	int n = (b - a) / h;
	double x[n+1];
	double y[n+1];
	x[0] = a;
	y[0] = 1;
	double k1, k2, k3, k4;

	ofstream fout("out.txt");

	for(int i = 1; i <= n; i++) {
		x[i] = a + i*h;
		k1 = h * f(x[i - 1], y[i - 1]);
        k2 = h * f(x[i - 1] + h / 2, y[i - 1] + k1 / 2);
        k3 = h * f(x[i - 1] + h / 2, y[i - 1] + k2 / 2);
        k4 = h * f(x[i - 1] + h, y[i - 1] + k3);
        y[i] = y[i - 1] + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
		fout << x[i-1] << " " << y[i-1] << "\n";
	}
	fout << x[n] << " " << y[n] << "\n";

}

double f(double x, double y)
{
	return x+y;
}
#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

double left_rectangle_method(double a, double b, double n);
double right_rectangle_method(double a, double b, double n);
double rectangle_method(double a, double b, double n);
double trapezoid_method(double a, double b, double n);
double Simpson_method(double a, double b, double n);
double f(double x);

int main()
{
	double a, b;
	int n;

	cout << "Введите нижний предел интегрирования: ";
	cin >> a;
	cout << "Введите верхний предел интегрирования: ";
	cin >> b;
	cout << "Введитем количество отрезков разбиения интервала: ";
	cin >> n;

	double left = left_rectangle_method(a,b,n);
	double right = right_rectangle_method(a,b,n);
	double rect = rectangle_method(a, b, n);
	double trapezoid = trapezoid_method(a, b, n);
	double Simpson = Simpson_method(a,b,n);

	ofstream fout("out.txt");
	
	fout <<"Левые прямоугольники: "<< left << "\n" 
	<< "Правые прямоугольники: " << right << "\n" 
	<< "Прямоугольники: " << rect << "\n"
	<< "Трапеции: " << trapezoid << "\n"
	<< "Симпсон: " << Simpson;

}

double left_rectangle_method(double a, double b, double n)
{
	double h = (b-a)/n;
	double I = 0;

	for(int i = 0; i < n; i++)
		I += h*f(a + i*h);

	return I;
}

double right_rectangle_method(double a, double b, double n)
{
	double h = (b-a)/n;
	double I = 0;

	for(int i = 1; i <= n; i++)
		I += h*f(a + i*h);

	return I;
}

double rectangle_method(double a, double b, double n)
{
	double h = (b-a)/n;
	double I = 0;

	for(int i = 0; i < n; i++)
		I += h*f(a + (i+0.5)*h);

	return I;
}

double trapezoid_method(double a, double b, double n)
{
	double h = (b-a)/n;
	double S = 0;

	for(int i = 1; i < n; i++)
		S += f(a + i*h);
	S = (h/2)*(f(a) + f(b) + 2*S);

	return S;
}

double Simpson_method(double a, double b, double n)
{
	double h = (b-a)/n;
	double S = f(a) + f(b);
	int k = 4;

	for(int i = 1; i < n; i++, k = 6-k)
		S += k*f(a + i*h);
	S *= h/3;

	return S; 
}

double f(double x)
{
	return (x + 1)*sin(x);
}
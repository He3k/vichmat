#include <iostream>
#include <fstream>

using namespace std;

void lagr(double *xout, double *yout, int i);
void bubble_sort(double* x,double* y,int n);
int main()
{
	int num_of_xy = 4;
	double xin[num_of_xy] = {1, 2, 3, 4};
	double yin[num_of_xy] = {1, 1.4142, 1.7321, 2};

	cout << "Введите кол-во иксов: ";
	int n;
	cin >> n;
	double *x = new double [n];
	cout << "Введите иксы:\n";
	for(int i = 0; i < n; i++)
		cin >> x[i];

	cout << "\n\n";

	double *xout = new double[num_of_xy + n];
	double *yout = new double[num_of_xy + n];

	for(int i = 0; i < num_of_xy; i++) {
		xout[i] = xin[i];
		yout[i] = yin[i];
	}


	for(int i = num_of_xy; i < num_of_xy + n; i++)
		xout[i] = x[i-num_of_xy];


	for(int i = num_of_xy; i < n + num_of_xy; i++) {
		lagr(xout, yout, i);
	}

	bubble_sort(xout, yout, n + num_of_xy);
	
	ofstream fout("out.txt");
	for(int i = 0; i < n + num_of_xy; i ++) {
		fout << xout[i] <<" " << yout[i] <<"\n";
	}

}

void lagr(double *xout, double *yout, int i)
{
	double c;

	for (int j = 0; j <	 i; j++) {
		c = 1;
		for (int k = 0; k<i; k++)
			{
				if (k != j)
				c *=(xout[i] - xout[k]) / (xout[j] - xout[k]);
			}
		yout[i] += yout[j] *c;
	}	
	


}

void bubble_sort(double* x,double* y,int n)
{
	float b;
	float c;
	for (int i=0; i < n - 1; i++)
	{
     	for (int j=0; j < n -1; j++) { 
         	if (x[j] > x[j+1])
         	{
        	 	b = x[j];
        	 	c = y[j];
        	 	x[j] = x[j+1];
        	 	y[j] = y[j+1];
        	 	x[j+1] = b;
        	 	y[j+1] = c;
			}
		}
	}
}
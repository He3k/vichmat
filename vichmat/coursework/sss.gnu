set termoption enhanced
set terminal png size 1600,1200 font "Arial, 16"
set output "plot2.png"
set style line 1 lc rgb "0xDC143C" lt 1 lw 4 pt 9 ps 1
set style line 2 lc rgb "0x6495ED" lt 1 lw 4 pt 7 ps 1
set border lw 2
set grid
#set key out right
set xlabel "Количество дней"
set ylabel "Количество людей" rotate by 90
#set xrange [-1: 10]
#set yrange [0: 10]
#set logscale y
set format x "%6.0f"
set format y "%.0f"
#set ytics 1
plot 'out.txt' using 1:5 with lines lw 2 title "Инфицированные с симптомами"
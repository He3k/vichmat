#include <iostream>
#include <fstream>

using namespace std;

void newton(double *x, double *y, int n);
double F(double* xin, double *yin, int s, int f);
void bubble_sort(double* x,double* y,int n);
int main()
{
	int num_of_xy = 4;
	double xin[num_of_xy] = {1, 2, 3, 4};
	double yin[num_of_xy] = {1, 1.4142, 1.7321, 2};
	// double xin[num_of_xy] = {1.5, 1.52, 1.54, 1.56, 1.58};
	// double yin[num_of_xy] = {4.4817, 4.5722, 4.6646, 4.7588, 4.855};
	cout << "Введите кол-во иксов: ";
	int n;
	cin >> n;
	double *x = new double [n];
	cout << "Введите иксы:\n";
	for(int i = 0; i < n; i++)
		cin >> x[i];

	cout << "\n\n";

	double *xout = new double[num_of_xy + n];
	double *yout = new double[num_of_xy + n];

	for(int i = 0; i < num_of_xy; i++) {
		xout[i] = xin[i];
		yout[i] = yin[i];
	}


	for(int i = num_of_xy; i < num_of_xy + n; i++)
		xout[i] = x[i-num_of_xy];


	for(int i = num_of_xy; i < num_of_xy + n; i++)
		newton(xout, yout, i);
	

	bubble_sort(xout, yout, n + num_of_xy);
	
	ofstream fout("out.txt");
	for(int i = 0; i < n + num_of_xy; i ++) {
		fout << xout[i] <<" " << yout[i] <<"\n";
	}

}

void newton(double *x, double *y, int n)
{
	double mult = 1, U;
	double result = y[0];

	for(int i = 1; i < n; i++) {
		mult *= (x[n] - x[i-1]);
		U = F(x, y, 0, i);
		result += mult*U;
	}
	
	y[n] = result;
}

double F(double* xin, double *yin, int s, int f)
{
	if(s == f)
		return yin[s];

	return (F(xin, yin, s+1, f) - (F(xin, yin, s, f-1))) / (xin[f] - xin[s]);
}

void bubble_sort(double* x,double* y,int n)
{
	float b;
	float c;
	for (int i=0; i < n - 1; i++)
	{
     	for (int j=0; j < n -1; j++) { 
         	if (x[j] > x[j+1])
         	{
        	 	b = x[j];
        	 	c = y[j];
        	 	x[j] = x[j+1];
        	 	y[j] = y[j+1];
        	 	x[j+1] = b;
        	 	y[j+1] = c;
			}
		}
	}
}
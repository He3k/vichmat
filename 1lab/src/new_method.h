#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void fill_matrix(char* check);
void print(int size, float matrix[][size+1]);
void check_swap(int size, float matrix[][size+1]);
void swap(int size, float matrix[][size+1], int i, int j, int index);
void calculation(int size, float matrix[][size+1], float *vars);
void out_in_file(int size, float *vars);
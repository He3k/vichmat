#include "new_method.h"


void fill_matrix(char* check) {
	int i,j,size;
    FILE *fi;                                   //
    if ((fi = fopen(check, "r")) != NULL) {     // check
	  while (!ferror(fi) && !feof(fi))  {       // 
        if (fgetc(fi) == '\n')                  // strings
        size++;                                 //
      }                                         // file
    	//printf("count >> %d\n", size);        
    	fclose(fi);                             //
	} else {                                    //
		printf("\033[1;31mWhich file should I open, genius?\033[0m\n");
		exit(0);                                //
	}                                           //

	float matrix[size][size + 1];               // init matrix

    FILE *file;                                 //
    file = fopen(check, "r");                   //
	for (i = 0; i < size; i++) {                // fill
		for (j = 0; j < size + 1; j++) {        // 
			fscanf(file,"%f",&matrix[i][j]);    // matrix
		}                                       //
	}                                           //
    fclose(file);
	print(size, matrix);
	check_swap(size, matrix);
	print(size, matrix);
	float vars[size];
	calculation(size, matrix, vars);
	out_in_file(size, vars);

}

void print(int size, float matrix[][size+1]) {
	int j;
    for (int i = 0; i < size; i++) {                //
		for (j = 0; j < size + 1; j++){         // 
			printf("  %.0f  ", matrix[i][j]);        // print
		}                                       // 
		printf("\n");                           // matrix
		j = 0;                                  //
	}                                           //
	printf("\n");
}

void check_swap(int size, float matrix[][size+1]) {
	int i, j, l, index;
	float t, mod;
	for (j = 0; j < size - 1; j++) {
		mod = matrix[j][j];                          
		index = j;                                   
		for (int i = j + 1; i < size; i++) {
			if (fabs(matrix[i][j]) > fabs(mod)) {
				mod = matrix[i][j];
				index = i;
			}
		}
		if (index != j) {
			for (int i = 0; i < size + 1; i++) {
				swap(size, matrix, i, j, index);
			}
			for (i = j + 1; i < size; i++) {
				t = matrix[i][j] / matrix[j][j] * (-1);

				for (l = 0; l <= size; l++) {
					matrix[i][l] = matrix[i][l] + t * matrix[j][l];
				}
			}

		} else {
			for (i = j + 1; i < size; i++) {
				t = matrix[i][j] / matrix[j][j] * (-1);

				for (l = 0; l <= size; l++) {
					matrix[i][l] = matrix[i][l] + t * matrix[j][l];
				}
				
			}
	
		}
	print(size, matrix);
	}
}

void swap(int size, float matrix[][size+1], int i, int j, int index) {
	float tmp;
	tmp = matrix[j][i];
	matrix[j][i] = matrix[index][i];
	matrix[index][i] = tmp; 
}

void calculation(int size, float matrix[][size+1], float *vars) {
	int i, j; 
	float t;
	for (i = size - 1; i >= 0; i--) {
		t = matrix[i][size];
		for (j = i + 1; j < size; j++) { 
			t = t - (vars[j] * matrix[i][j]);
		}
		t = t / matrix[i][i];
		vars[i] = t;
	}
}

void out_in_file(int size, float *vars) {
	FILE *file_out;
	file_out = fopen("out.txt", "w+");
	printf("Result: \n" );
  	for (int i = 0; i < size; i++) {
		printf ("X%d =  %.0f\n", i + 1, vars[i]);
        fprintf(file_out, "x =%6.2f\n", vars[i]);
    }
    fclose(file_out); 
}